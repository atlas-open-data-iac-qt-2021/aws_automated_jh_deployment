variable "username" {
  description = "The username for the DB master user"
  type        = string
}

variable "password" {
  description = "The password for the DB master user"
  type        = string
}

#variable "hostname" {
#  description = "The name of the VM created in openstack"
#  type        = string
#}

variable "security_group" {
  description = "The security group of the VM created in aws"
  type        = string
}

# variable "sshkey" {
#   description = "The name of the ssh key created for the instance"
#   type        = string
# }

variable "dnsrecord" {
  description = "The DNS related to the certificate"
  type        = string
}

variable "sshkeyname" {
  description = "The VM's public sshkey name created in aws"
  type        = string
}

variable "region" {
  description = "The name of the region"
  type        = string
}


variable "image" {
  description = "The AMI of the AWS instance"
  type        = string
}

variable "flavor" {
  description = "The flavor of the AWS instance"
  type        = string
}




