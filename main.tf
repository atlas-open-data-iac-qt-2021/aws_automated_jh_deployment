  provider "aws" {
	region = var.region
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

resource "aws_key_pair" "ssh-key" {
  key_name   = var.sshkeyname
  public_key = tls_private_key.this.public_key_openssh
  #public_key = var.sshkey
}


  #=====================================================================================================

  #setup of the VM instance considering the specific parameters of openstack cern
  resource "aws_instance" "example" {
  ami           = var.image
  instance_type = var.flavor
  security_groups    = ["${aws_security_group.example.name}"]
  key_name        = "${aws_key_pair.ssh-key.key_name}"		
  associate_public_ip_address = true
  user_data = "${file("user_data.txt")}"



  # Copies the config folder to the instance
    provisioner "file" {
      source      = "conf/"
      destination = "."

      #set the type of connection with the user and the psw previoulsy set
      connection {												
        type     = "ssh"
        user     = var.username
        password = var.password
        host     = "${aws_instance.example.public_ip}"
      }  
    }

  provisioner "remote-exec" {       									
      inline = [
  #	  "sudo touch /boot/grub/menu.lst",
  #	  "sudo update-grub2",
  #	  "sudo apt update",
  #	  "sudo apt --yes upgrade",
  #	  "sudo apt update",
      "echo ==========================",
      "echo Installing Zerotier",
      "echo ==========================",
      "curl -s https://install.zerotier.com | sudo bash",
      "sudo zerotier-cli join 8286ac0e47bce2df",
      "echo ==========================",
      "echo Installing Docker",
      "echo ==========================",
      "sudo apt --yes install docker.io",
      "echo ==========================",
      "echo Installing Make",
      "echo ==========================",
      "sudo apt --yes install make",
      "echo ==========================",
      "echo Installing docker-compose",
      "echo ==========================",
      "sudo apt --yes install docker-compose",
      "mkdir secrets",
      "echo ==========================",
      "echo Creating cetificate",
      "echo ==========================",
      "sudo openssl req -x509 -newkey rsa:4096 -keyout ./secrets/jupyterhub.key -out ./secrets/jupyterhub.crt -days 365 -nodes -subj \"/CN=${var.dnsrecord}\" ",
      "echo ==========================",
      "echo Make build",
      "echo ==========================",
      "sudo make build",
      "echo ==========================",
      "echo Make notebook",
      "echo ==========================",
#      "export DOCKER_NOTEBOOK_IMAGE=${var.notebook}",
      "sudo make notebook_image",
      "echo ==========================",
      "echo docker-compose up",
      "echo ==========================",
      "sudo docker-compose up -d"    			
          ]

    #set the type of connection with the user and the psw previoulsy set
    connection {												
      type     = "ssh"
      user     = var.username
      password = var.password
      host     = "${aws_instance.example.public_ip}"
    }  
  }
  }

resource "aws_security_group" "example" {
  name        = var.security_group
  description = "ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 }


output "instance_ip_addr" {
  value = aws_instance.example.public_ip
}
