echo ""
cat misc/title.txt
echo ""
if [[ $(aws --version) ]]; then
    echo ""
else
    echo "installing the AWS CLI..."
	if ! [[ $(unzip --version) ]]; then
		sudo apt --yes install unzip
	fi
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install
fi

echo "Configuring the AWS provider..."
/usr/local/bin/aws configure

# Set the color variable
green='\033[0;32m'
red='\e[1;31m'
# Clear the color after that
clear='\033[0m'

if [ -a user_data.txt ]
then 
 rm user_data.txt
fi


echo ""
echo "===================================================================="
TF_VAR_username=$(sed -nr "/^\[INSTANCE\]/ { :l /^username[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_username=${TF_VAR_username//[[:blank:]]/}
echo -e "Name for the user:  ${red}$TF_VAR_username${clear}";

echo ""
echo "===================================================================="
TF_VAR_password=$(sed -nr "/^\[INSTANCE\]/ { :l /^password[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_password=${TF_VAR_password//[[:blank:]]/}
echo -e -n "Password for the instance created:  ${red}$TF_VAR_password${clear}";


echo ""
echo "===================================================================="
TF_VAR_sshkeyname=$(sed -nr "/^\[INSTANCE\]/ { :l /^sshkey[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_sshkeyname=${TF_VAR_sshkeyname//[[:blank:]]/}
echo -e "Name for the sshkey :  ${red}$TF_VAR_sshkeyname${clear}";

echo ""
echo "===================================================================="
TF_VAR_image=$(sed -nr "/^\[INSTANCE\]/ { :l /^image[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_image=${TF_VAR_image//[[:blank:]]/}
echo -e "Name of the image used for the instance:  ${red}$TF_VAR_image${clear}";

echo ""
echo "===================================================================="
TF_VAR_flavor=$(sed -nr "/^\[INSTANCE\]/ { :l /^flavor[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_flavor=${TF_VAR_flavor//[[:blank:]]/}
echo -e "Flavor used for the instance:  ${red}$TF_VAR_flavor${clear}";
echo "------------------------------------------"
echo "t2.micro     (AWS free tier)"
echo -e "t2.large      (${green}recommended${clear})"
echo "------------------------------------------"

echo ""
echo "===================================================================="
TF_VAR_dnsrecord=$(sed -nr "/^\[CERTIFICATE\]/ { :l /^dnsrecord[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_dnsrecord=${TF_VAR_dnsrecord//[[:blank:]]/}
echo -e "Domain to be used for the certificate:  ${red}$TF_VAR_dnsrecord${clear}";

echo ""
echo "===================================================================="
TF_VAR_security_group=$(sed -nr "/^\[CONNECTION\]/ { :l /^security_group[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_security_group=${TF_VAR_security_group//[[:blank:]]/}
echo -e "Security group used:  ${red}$TF_VAR_security_group${clear}";


echo ""
echo "===================================================================="
#export TF_VAR_notebook_image=$(awk -F "=" '/notebook/ {print $2}' config.ini)
DOCKER_NOTEBOOK_IMAGE=$(sed -nr "/^\[JUPYTERHUB\]/ { :l /^notebook[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
DOCKER_NOTEBOOK_IMAGE=${DOCKER_NOTEBOOK_IMAGE//[[:blank:]]/}
replacement=$(sed -nr "/ / { :l /^DOCKER_NOTEBOOK_IMAGE[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./conf/.env)
replacement=${replacement//[[:blank:]]/}
sed -i "s@$replacement@$DOCKER_NOTEBOOK_IMAGE@g" ./conf/.env
echo -e "Docker image used for the notebooks:  ${red}$DOCKER_NOTEBOOK_IMAGE${clear}";


#echo ""
#echo "===================================================================="
#echo "Creating public key for AWS..."
#aws ec2 create-key-pair --key-name MyAWSKeyPair --query 'KeyMaterial' --output text > MyAWSKeyPair.pem
#chmod 400 MyAWSKeyPair.pem
#TF_VAR_sshkey1=`ssh-keygen -y -f MyAWSKeyPair.pem` 
#echo "Your public key is: " 
#cho "$TF_VAR_sshkey1"
#export TF_VAR_sshkey=$(echo "$TF_VAR_sshkey1")

export TF_VAR_region=$(aws configure get region)



#****************************************** Create the conf file user_data.txt
echo "#cloud-config">>user_data.txt
echo "password: ${TF_VAR_password}">>user_data.txt
echo "chpasswd: { expire: False }">>user_data.txt
echo "ssh_pwauth: True">>user_data.txt


echo ""
echo "===================================================================="
echo "===================================================================="
read -r -p "Do you confirm this configuration? (yes/no) " confirmation


if [ $confirmation == 'yes' ]
then 
 if ! [[ $(terraform -version) ]]; then
	sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
	curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
	sudo apt-get update && sudo apt-get --yes install terraform
 fi	
 echo "Commencing deployment..."
 terraform 0.13upgrade .
 terraform init
 terraform plan
 terraform apply
 instance_ip=$(terraform output -json instance_ip_addr | jq -r '.[0]')
 echo -e "To access the JupyterHub, use this URL: ${green}https://$instance_ip/hub/login${clear}"
fi
